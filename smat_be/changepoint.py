#!/usr/bin/env python
"""Changepoint analysis by binary segmentation.

paper: Taylor, Wayne A - Change-Point Analysis:A Powerful New Tool For Detecting Changes
"""
import json
from typing import Dict, Optional, List
from datetime import datetime
from collections import defaultdict
from pandas import DataFrame, concat  # type: ignore
import numpy as np  # type: ignore
from requests.models import Response


def mk_df(response: Response, created: str = "reddit") -> DataFrame:
    """Take a 200 response and make a dataframe."""
    try:
        content = response.json()["aggregations"][created]["buckets"]
    except Exception as exception:
        raise exception

    for i in content:
        del i["key_as_string"]

    def transform(inp: List[Dict[str, int]]) -> Dict[str, List[int]]:
        out: Dict[str, List[int]] = defaultdict(list)
        for i in inp:
            for key, val in i.items():
                out[key].append(val)

        return out

    dat = DataFrame(transform(content))

    return dat.assign(
        date=dat.key.apply(lambda x: datetime.fromtimestamp(x // 1000))
    ).rename(columns={"doc_count": "usage"})


class BaseData:
    """Give a time series w indices called `key` and data called `usage`.

    Indices are in epoch time * 1000.
    """

    def __init__(self, dat: DataFrame) -> None:
        """Init BaseData."""
        self.dataframe = dat
        self.values = self.dataframe.usage.values
        self.cusum0 = self.cusum(self.dataframe)
        self.sdiff0 = self.sdiff(self.cusum0)
        # an initial estimate of the changepoint
        self.changepoint: Optional[int] = max(
            self.cusum0.items(), key=lambda x: abs(x[1])
        )[0]

    @staticmethod
    def cusum(dat: DataFrame) -> Dict[int, float]:
        """Produce a cumulative sum of residuals.

        cusums[0] = 0
        cusums[n+1] = cusums[n] + (X[n+1] - X.mean())
        """
        xbar = dat.usage.mean()
        cusums: Dict[int, float] = dict()
        prev = dat.key.values[0] - 86400000
        cusums[prev] = 0
        for row in dat.values:
            epoch_key = row[0]
            usage = row[1]
            resid = usage - xbar
            cusums[epoch_key] = cusums[prev] + resid
            prev = epoch_key
        return cusums

    @staticmethod
    def sdiff(cusum: Dict[int, float]) -> float:
        """Take the distance between max and min of cusum.

        cusum.max() - cusum.min().
        """
        dat = DataFrame(cusum.items()).rename(columns={0: "index", 1: "cumulative_sum"})
        return dat.cumulative_sum.max() - dat.cumulative_sum.min()


class BinarySegmentation(BaseData):
    """Do binary segmentation once to find one changepoint and two segments."""

    def __init__(self, dat: DataFrame, num_bootstraps: int) -> None:
        """Initialize BinarySegmentation.

        If bootstraps return confidence that a changepoint is appropriate, add labels to dataframe.
        """
        self.changepoint: Optional[int]
        super().__init__(dat)
        self.bootstrap_counter = 0
        self.num_bootstraps = num_bootstraps
        self.confidence = self.do_bootstrap()
        if self.confidence > 0.95:
            self.changepoint = self.minimize_mse()
            self.dataframe = self.dataframe.assign(label=self.label().label)

        else:
            self.changepoint = None
            print(
                "we are not confident that cpa is relevant (bootstrapconfidence <= 0.95)"
            )

    def bootstrap(self) -> None:
        """Sample the time series order to check changes in the cumulative sum."""
        btstrp = self.dataframe.sample(frac=1)
        sdiff1 = self.sdiff(self.cusum(btstrp))
        if sdiff1 < self.sdiff0:
            self.bootstrap_counter += 1

    def do_bootstrap(self) -> float:
        """Do the specified number of samples and return confidence."""
        for _ in range(self.num_bootstraps):
            self.bootstrap()

        return self.bootstrap_counter / self.num_bootstraps

    def modified_mse(self, changepoint: int) -> float:
        """Squared error modified for a changepoint."""
        x_left = self.dataframe.usage[self.dataframe.key <= changepoint]
        x_right = self.dataframe.usage[self.dataframe.key > changepoint]
        xbar_left = x_left.mean()
        xbar_right = x_right.mean()

        return ((x_left - xbar_left) ** 2).sum() + ((x_right - xbar_right) ** 2).sum()

    def minimize_mse(self) -> int:
        """Brute force argmin."""
        curr_min: float = 2 ** 64
        curr_argmin: int = 0
        for key in self.dataframe.key:
            mse = self.modified_mse(key)
            if mse < curr_min:
                curr_min = mse
                curr_argmin = key
        return curr_argmin

    def label(self) -> DataFrame:
        """Label the dataframe with your segments.

        To be done after minimize_mse set self.changepoint to the correct value.
        """
        left = self.dataframe[self.dataframe.key <= self.changepoint].assign(
            label=lambda x: "Before the changepoint"
        )
        right = self.dataframe[self.dataframe.key > self.changepoint].assign(
            label=lambda x: "After the changepoint"
        )

        return concat((left, right))


def changepoint_main(response: Response, created: str):
    """Take a timeline response from pushshift and return its changepoint."""
    dat = mk_df(response, created)
    changepoint_analysis = BinarySegmentation(
        dat, max(int(dat.shape[0] * np.log(dat.shape[0]) / 8), dat.shape[0])
    )
    dat = changepoint_analysis.dataframe
    _changepoint = changepoint_analysis.changepoint

    if _changepoint:
        dat["doc_count"] = dat["usage"]
        dat["key_as_string"] = dat["key"].map(lambda x: str(x)[0:10])
        before = dat[dat["label"] == "Before the changepoint"][
            ["key", "key_as_string", "doc_count"]
        ].to_json(orient="records")
        after = dat[dat["label"] != "Before the changepoint"][
            ["key", "key_as_string", "doc_count"]
        ].to_json(orient="records")
    else:
        _changepoint = None
        before = "[]"
        after = "[]"

    changepoint_data = {
        "change_date": _changepoint,
        created: {"before": json.loads(before), "after": json.loads(after)},
    }
    return changepoint_data
