#!/usr/bin/env python

from test_base import TestBase


class TestTimeseries(TestBase):
    """Tests the timeseries route.

    ROUTES: /timeseries.
    """

    def test_timeseries_reddit(self) -> None:
        """Verifies that the /timeseries is returning 200 when pinging reddit."""
        test_case = {
            "term": "tornado",
            "site": "reddit",
            "interval": "month"
            # limit, since, until left blank
        }
        test_url = "/timeseries?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)

    def test_timeseries_twitter(self) -> None:
        """Verifies that the /timeseries is returning 200 when pinging twitter."""
        test_case = {
            "term": "tornado",
            "site": "twitter",
            "interval": "month",
            "since": "fish",
            "until": "",
        }
        test_url = "/timeseries?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)

    def test_timeseries_gab(self) -> None:
        """Verifies that the /timeseries is returning 200 when pinging gab."""
        test_case = {
            "term": "election",
            "site": "gab",
            "interval": "week",
            "since": "2020-01-19",
            "until": "none",
        }
        test_url = "/timeseries?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)
