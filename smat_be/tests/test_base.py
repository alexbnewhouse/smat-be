#!/usr/bin/env python
from smat_be import APP
from unittest import TestCase
import os


class TestBase(TestCase):
    """forms the base test_client app instance.

    to be inherited by tests of all routes.
    """

    def setUp(self) -> None:
        """Runs before the test."""

        APP.config["TESTING"] = True
        APP.config["WTF_CSRF_ENABLED"] = False

        # self.HEADERS = {"content-type": "application/json"}
        self.APP = APP.test_client()
