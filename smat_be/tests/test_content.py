#!/usr/bin/env python

from test_base import TestBase


class TestContent(TestBase):
    """Tests the content route.

    ROUTES: /content.
    """

    def test_content_reddit(self) -> None:
        """Verifies that the /content route is returning 200 and 4 comments when pinging reddit at limit=4."""
        limit = 4
        test_case = {
            "term": "tornado",
            "site": "reddit",
            "limit": limit
            # limit, since, until left blank
        }
        test_url = "/content?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json["hits"]["hits"]), limit)

    def test_content_twitter(self) -> None:
        """Verifies that the /content route is returning 200 and 1 comment when pinging twitter at limit=1."""
        limit = 1
        test_case = {
            "term": "tornado",
            "site": "twitter",
            "interval": "month",
            "since": "fish",
            "until": "",
            "limit": limit,
        }
        test_url = "/content?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json["hits"]["hits"]), limit)

    def test_content_gab(self) -> None:
        """Verifies that the /content is returning 200 and 3 comments when pinging gab at limit=3."""
        limit = 3
        test_case = {
            "term": "election",
            "site": "gab",
            "interval": "week",
            "since": "2020-01-19",
            "until": "none",
            "limit": limit,
        }
        test_url = "/content?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json["hits"]["hits"]), limit)
