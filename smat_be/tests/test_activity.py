#!/usr/bin/env python
"""Test the aggregatation of subreddits or authors"""

from test_base import TestBase


class TestActivity(TestBase):
    """Tests the activity route.

    ROUTES: /activity.
    """

    def test_activity_reddit_by_subreddit(self) -> None:
        """Verifies that the /activity is returning 200 when agging over subreddit."""

        test_case = {
            "term": "election",
            "site": "reddit",
            "agg_by": "subreddit",
        }
        test_url = "/activity?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)

    def test_activity_reddit_by_author(self) -> None:
        """Verifies that the /activity is returning 200 when agging over author on reddit."""
        test_case = {
            "term": "tornado",
            "agg_by": "author",
            "site": "reddit",
            "since": "pizza",
            "until": "",
        }
        test_url = "/activity?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)

    def test_activity_twitter_by_author(self) -> None:
        """Verifies that the /activity is returning 200 when agging over author on twitter."""
        test_case = {
            "term": "tornado",
            "site": "twitter",
            "agg_by": "author",
            "since": "2020-01-15",
            "until": "2020-01-31",
        }
        test_url = "/activity?" + "&".join([f"{k}={v}" for k, v in test_case.items()])

        response = self.APP.get(test_url)

        self.assertEqual(response.status_code, 200)
