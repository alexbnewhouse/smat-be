#!/usr/bin/env python
"""The SMAT backend app just makes Elasticsearch's inputs a ton easier to work with.

While retaining all the complexity/perplexity of Elasticsearch output.
"""
import json
import os
from json.decoder import JSONDecodeError
from typing import Dict, Any
from flask import Flask, request, jsonify  # type: ignore
from flask.wrappers import Response  # type: ignore
from flask_cors import cross_origin  # type: ignore
from smat_be.constants import SINCE, UNTIL
from smat_be.utils import mk_since_until
from smat_be.pushshift_queries import (
    content_via_pushshift,
    time_series_via_pushshift,
    activity_via_pushshift,
)

APP = Flask(__name__)

###############################################
#                  content                  ###
###############################################


@APP.route("/content", methods=["GET"])
@cross_origin()
def content() -> Response:
    """Retrieve a list of post objects."""
    limit = request.args.get("limit", type=int, default=10)
    site = request.args.get("site", type=str, default="reddit")
    term = request.args.get("term", type=str, default=None)
    since_ = request.args.get("since", type=str)
    until_ = request.args.get("until", type=str)
    # returns a tuple (y, m, d)
    since = mk_since_until(since_) or SINCE
    until = mk_since_until(until_) or UNTIL

    if not term:
        return APP.response_class(
            response=json.dumps({"FAIL": 'You need to submit the "term" key.'}),
            status=200,
            mimetype="application/json",
        )

    pushshift_response, created_key, content_key = content_via_pushshift(
        term=term, site=site, limit=limit, since=since, until=until
    )

    response: Dict[str, Any]
    if pushshift_response.status_code == 200:
        try:
            _ = pushshift_response.json()["hits"]["hits"]
        except JSONDecodeError as exception:
            response = {
                "FAIL": "Cant Decode JSON from response",
                "message": f"{exception}",
            }
            return APP.response_class(response=response, status=500)
        except KeyError:
            response = {
                "FAIL": "PushShift responded with a 200, but cant parse hits",
                "pushshift_response_json": pushshift_response.json(),
            }
            return APP.response_class(response=response, status=500)

        else:
            response = {
                **{"created_key": created_key, "content_key": content_key},
                **pushshift_response.json(),
            }
    else:
        response = {
            "FAIL": "Pushshift did not respond with 200",
            "status_code": pushshift_response.status_code,
            "response_content": str(pushshift_response.content),
        }
        return APP.response_class(response=response, status=500)

    return APP.response_class(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


###############################################
#                   time series             ###
###############################################
@APP.route("/timeseries", methods=["GET"])
@cross_origin()
def timeseries() -> Response:
    """Retrieve a time series."""
    site = request.args.get("site", type=str, default="reddit")
    term = request.args.get("term", type=str, default=None)
    interval = request.args.get("interval", type=str, default="day")
    since_ = request.args.get("since", type=str)
    until_ = request.args.get("until", type=str)
    changepoint_ = request.args.get("changepoint", type=bool, default=False)
    # returns a tuple (y, m, d)
    since = mk_since_until(since_) or SINCE
    until = mk_since_until(until_) or UNTIL

    if not term:
        return APP.response_class(
            response=json.dumps({"FAIL": 'You need to submit the "term" key.'}),
            status=200,
            mimetype="application/json",
        )

    pushshift_response, created_key = time_series_via_pushshift(
        term=term,
        site=site,
        interval=interval,
        since=since,
        until=until,
        changepoint=changepoint_,
    )

    if pushshift_response.status_code == 200:
        try:
            _ = pushshift_response.json()["aggregations"][created_key]["buckets"]
        except JSONDecodeError as exception:
            response = {
                "FAIL": "Cant Decode JSON from response",
                "message": f"{exception}",
            }
            return APP.response_class(response=response, status=500)
        except KeyError:
            response = {
                "FAIL": "PushShift responded with a 200, but cant parse aggregations",
                "pushshift_response_json": pushshift_response.json(),
            }
            return APP.response_class(response=response, status=500)
        else:
            response = {**{"created_key": created_key}, **pushshift_response.json()}  # type: ignore
    else:
        response = {
            "FAIL": "Pushshift did not respond with 200",
            "status_code": pushshift_response.status_code,  # type: ignore
            "response_content": str(pushshift_response.content),
        }
        return APP.response_class(response=response, status=500)

    return APP.response_class(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


###############################################
#               activity                    ###
###############################################
@APP.route("/activity", methods=["GET"])
@cross_origin()
def activity() -> Response:
    """Aggregate over reddit by either subreddit or author."""
    site = request.args.get("site", type=str, default="reddit")
    term = request.args.get("term", type=str, default=None)
    agg_by = request.args.get("agg_by", type=str, default="author")
    since_ = request.args.get("since", type=str)
    until_ = request.args.get("until", type=str)
    # returns a tuple (y, m, d)
    since = mk_since_until(since_) or SINCE
    until = mk_since_until(until_) or UNTIL

    if not term:
        return APP.response_class(
            response=json.dumps({"FAIL": 'You need to submit the "term" key.'}),
            status=200,
            mimetype="application/json",
        )

    if site == "twitter" and agg_by == "subreddit":
        msg = "You tried to aggregate over subreddits on twitter. Twitter doesn't have subreddits."
        return APP.response_class(response=json.dumps({"FAIL": msg}), status=500)
    if site == "twitter":
        agg_by = "screen_name"
    if site == "gab":
        agg_by = "account.acct"
    if site == "4chan":
        agg_by = "name"
    if site == "8kun":
        agg_by = "name"

    pushshift_response = activity_via_pushshift(
        term=term, site=site, agg_by=agg_by, since=since, until=until
    )

    response: Dict[str, Any]
    if pushshift_response.status_code == 200:
        try:
            _ = pushshift_response.json()["aggregations"][agg_by]["buckets"]
        except JSONDecodeError as exception:
            response = {
                "FAIL": "PushShift responded with a 200, but cant extract json",
                "message": f"{exception}",
            }
            return APP.response_class(response=response, status=500)
        except KeyError:
            response = {
                "FAIL": "PushShift responded with a 200, but cant parse aggregations",
                "pushshift_response_json": pushshift_response.json(),
            }
            return APP.response_class(response=response, status=500)
        else:
            response = pushshift_response.json()
            response["aggby_key"] = agg_by
    else:
        response = {
            "FAIL": "Pushshift did not respond with 200",
            "status_code": pushshift_response.status_code,
            "response_content": str(pushshift_response.content),
        }
        return APP.response_class(response=response, status=500)

    return APP.response_class(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


@APP.route("/")
def api_spec():
    """Render API swagger docs."""
    with open(
        os.path.dirname(os.path.realpath(__file__)) + "/swagger.json"
    ) as data_file:
        data = json.load(data_file)

    return jsonify(data), 200


def start():
    """Run SMAT-be in dev mode."""
    APP.run(host=os.getenv("FLASK_HOST"), port=os.getenv("PORT"))
