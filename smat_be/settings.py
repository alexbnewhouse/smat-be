#!/usr/bin/env python
"""settings works in concert with the .env file/environment variables."""
import os

from dotenv import load_dotenv  # type: ignore

load_dotenv()

REDDIT_COMMENT_SEARCH = os.getenv("REDDIT_COMMENT_SEARCH")
TWITTER_VERIFIED_SEARCH = os.getenv("TWITTER_VERIFIED_SEARCH")
TWITTER_VERIFIED_SEARCH = os.getenv("TWITTER_VERIFIED_SEARCH")
GAB_POST_SEARCH = os.getenv("GAB_POST_SEARCH")
FOUR_CHAN_SEARCH = os.getenv("FOUR_CHAN_SEARCH")
EIGHT_CHAN_SEARCH = os.getenv("EIGHT_CHAN_SEARCH")
