#!/usr/bin/env python

"""Define some constants for the app."""
from datetime import date
from dateutil.relativedelta import relativedelta
from smat_be.utils import default_mk_since_until

HEADERS = {
    "Content-Type": "application/json",
    "Accept-Encoding": "deflate, compress, gzip",
}


TODAY = date.today()
UNTIL = default_mk_since_until(str(TODAY))
SINCE = default_mk_since_until(str(TODAY - relativedelta(months=2)))
