#!/usr/bin/env python
"""Utility functions for the smat back end."""
import re
from typing import Dict, Any, Optional, Tuple
from datetime import datetime
from collections import defaultdict
from smat_be.settings import (
    REDDIT_COMMENT_SEARCH,
    TWITTER_VERIFIED_SEARCH,
    GAB_POST_SEARCH,
    FOUR_CHAN_SEARCH,
    EIGHT_CHAN_SEARCH,
)


def dict_factory() -> Dict[str, Any]:
    """Create a nested dict for json purposes."""
    return defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    )


def mk_since_until(date: Optional[str]) -> Optional[Tuple[int, int, int]]:
    """Transform input into needed format.

    if input is missing or incorrect, return None.
    """
    rpat = r"\d\d\d\d[-/]\d\d[-/]\d\d"
    if date and re.match(rpat, date):
        return int(date[:4]), int(date[5:7]), int(date[8:10])
    return None


def default_mk_since_until(date: str) -> Tuple[int, int, int]:
    """Transform input into needed format.

    if input is missing or incorrect, return None.
    """
    # return int(date[:4]), int(date[5:7]), int(date[8:])
    return int(date[:4]), int(date[5:7]), int(date[8:10])


def dt_to_epoch(date: datetime) -> str:
    """Datetime object to epoch time."""
    return date.strftime("%s")


def map_site_to_query(site: str) -> Tuple:
    """Map a site string to query args."""
    sqm = {
        "reddit": {
            "created": "created_utc",
            "url": REDDIT_COMMENT_SEARCH,
            "content": "body",
            "dt_format": "epoch",
        },
        "4chan": {"created": "now", "url": FOUR_CHAN_SEARCH, "content": "html_parsed_com"},
        "8kun": {"created": "time", "url": EIGHT_CHAN_SEARCH, "content": "htmlparsedcom"},
        "twitter": {
            "created": "created_at",
            "url": TWITTER_VERIFIED_SEARCH,
            "content": "text",
        },
        "gab": {"created": "created_at", "url": GAB_POST_SEARCH, "content": "body"},
    }
    msqm = sqm[site]
    return (msqm["created"], msqm["content"], msqm["url"])
