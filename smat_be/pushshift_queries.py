#!/usr/bin/env python
"""Here is where the functions which ping Pushshift are defined."""
import json
from datetime import datetime
from typing import Tuple, Optional
from copy import deepcopy
import requests
from requests.models import Response
from smat_be.changepoint import changepoint_main
from smat_be.constants import HEADERS
from smat_be.utils import dict_factory, dt_to_epoch, map_site_to_query


def construct_date_query(query, created, since, until) -> dict:
    """
    Create a query with a date range.

    Take a query, created field and date range and
    return a new query with the specific date range.
    """
    date_query = deepcopy(query)
    _since = dt_to_epoch(datetime(*since))
    _until = dt_to_epoch(datetime(*until))

    date_query["query"]["bool"]["must"].append({"range": {created: {"gt": _since}}})
    date_query["query"]["bool"]["must"].append({"range": {created: {"lt": _until}}})
    return date_query


def content_via_pushshift(
    term: str,
    site: str,
    limit: int,
    since: Tuple[int, int, int],
    until: Tuple[int, int, int],
) -> Tuple[Response, Optional[str], Optional[str]]:
    """Use Elasticsearch to aggregate over time created.

    the variable limit should be set to zero if you only want time series data.
    """
    if limit > 10000:
        limit = 10000
    elif limit < 0:
        limit = 0

    created, content, url = map_site_to_query(site)
    if site == "reddit":
        HEADERS["referer"] = "redditsearch.io"

    query = dict_factory()
    query["query"]["bool"]["must"] = list()
    query["size"] = limit

    query["query"]["bool"]["must"].append(
        {"query_string": {"query": f'"{term}"', "default_field": content}}
    )
    date_query = construct_date_query(query, created, since, until)

    if site in ["4chan", "8kun"]:
        response = requests.get(
            str(url), data=json.dumps(date_query), headers=HEADERS, verify=False
        )
    else:
        response = requests.get(str(url), data=json.dumps(date_query), headers=HEADERS)

    return response, created, content


def time_series_via_pushshift(
    term: str,
    site: str,
    interval: str,
    changepoint: bool,
    since: Tuple[int, int, int],
    until: Tuple[int, int, int],
) -> Tuple[Response, Optional[str]]:
    """Use Elasticsearch to aggregate over time created.

    the variable limit should be set to zero if you only want time series data.
    """
    created, content, url = map_site_to_query(site)
    if site == "reddit":
        HEADERS["referer"] = "redditsearch.io"

    query = dict_factory()
    query["query"]["bool"]["must"] = list()
    query["aggs"][created]["date_histogram"]["field"] = created
    query["aggs"][created]["date_histogram"]["order"]["_key"] = "asc"
    query["aggs"][created]["date_histogram"]["interval"] = interval
    query["size"] = 0

    query["query"]["bool"]["must"].append(
        {"query_string": {"query": f'"{term}"', "default_field": content}}
    )
    date_query = construct_date_query(query, created, since, until)

    if site in ["4chan", "8kun"]:
        response = requests.get(
            str(url), data=json.dumps(date_query), headers=HEADERS, verify=False
        )
    else:
        response = requests.get(str(url), data=json.dumps(date_query), headers=HEADERS)

    if (changepoint) & (response.status_code == 200):
        if site == "reddit":
            total_hits = response.json()["hits"]["total"]
        else:
            total_hits = response.json()["hits"]["total"]["value"]
        if total_hits > 0:
            changepoint_data = changepoint_main(response, created)
            response_data = response.json()
            response_data["aggregations"]["changepoint"] = changepoint_data
            response = Response()
            response._content = json.dumps(response_data).encode()  # type: ignore
            response.status_code = 200

    return response, created


def activity_via_pushshift(
    term: str,
    site: str,
    agg_by: str,
    since: Tuple[int, int, int],
    until: Tuple[int, int, int],
) -> Response:
    """Use Elasticsearch via PushShift to aggregate over subreddit or author."""
    created, content, url = map_site_to_query(site)
    if site == "reddit":
        HEADERS["referer"] = "redditsearch.io"

    query = dict_factory()

    query["query"]["bool"]["must"] = list()

    query["query"]["bool"]["must"].append(
        {"query_string": {"query": f'"{term}"', "default_field": content}}
    )
    query["aggs"][agg_by]["terms"]["field"] = f"{agg_by}.keyword"
    query["aggs"][agg_by]["terms"]["execution_hint"] = "map"
    query["size"] = 0

    _since = dt_to_epoch(datetime(*since))
    date_query = construct_date_query(query, created, since, until)

    if site in ["4chan", "8kun"]:
        response = requests.get(
            str(url), data=json.dumps(date_query), headers=HEADERS, verify=False
        )
    else:
        response = requests.get(str(url), data=json.dumps(date_query), headers=HEADERS)

    return response
