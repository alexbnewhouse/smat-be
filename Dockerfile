FROM python:3.8-slim-buster

WORKDIR /code

EXPOSE $PORT

COPY . .

RUN pip install -e .
CMD smat