OK_COLOR=\033[32;01m
NO_COLOR=\033[0m
run:
	@printf "$(OK_COLOR)Running SMAT backend...$(NO_COLOR)\n"
	@smat-be
	@printf "$(OK_COLOR)OK$(NO_COLOR)\n"

install-dev:
	@printf "$(OK_COLOR)Installing dev version$(NO_COLOR)\n"
	@pip install -e .
	@printf "$(OK_COLOR)OK$(NO_COLOR)\n"

test:
	@printf "$(OK_COLOR)Running tests$(NO_COLOR)\n"
	@nose2 -v
	@printf "$(OK_COLOR)OK$(NO_COLOR)\n"

