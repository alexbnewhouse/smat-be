# Documentation

## The base url is `https://smat-be.herokuapp.com`. 

To see a sample of the data check-out: [https://smat-be.herokuapp.com/content?&term=election&site=twitter](https://smat-be.herokuapp.com/content?&term=election&site=twitter).

An app on Heroku's free tier "goes to sleep" when inactive and 
takes a few seconds to "wake up", but it stays awake
long enough that you should only notice this like once a day. 


## Along the route `/content`
please send a `GET` with the following url params:

| KEY      | VALUE                                          |
| -------- | ----------------------------------------       |
| term     | any string your user is interested in          |
| site     | either "reddit", "twitter", "4chan, or "8kun"  |
| limit    | an integer 0<=x<=10000                         | 
| since    | string YYYY-MM-DD or YYYY/MM/DD                |
| until    | string YYYY-MM-DD or YYYY/MM/DD                |

You will receive a JSON object with lots of stuff in it. 

if you dive into it `obj.hits.hits` you'll get a list of either **comment
objects** or **tweet objects**. If you're looking at reddit, see below for details
about the comment object. If you're looking at twitter, `tweet_obj._source.text` gets
the body of the tweet. If you're looking at Gab, `body` is the content keyword instead of `text`. 

#### Notes: 
- If you submit an out of bounds limit it will truncate it to `[0,10000]`. 
- this is the route you use if you wanna do like NLP, and you'll definitely want
  limit > 0.
  
## Along the route `/timeseries` 
please send a `GET` with the following url params:
    
| KEY      | VALUE                                           |
| -------- | ----------------------------------------        |
| term     | any string your user is interested in           |
| site     | either "reddit", "twitter", "8kun", or "4chan"  |
| interval | either "hour", "day", "week", or "month"        |
| since    | string YYYY-MM-DD or YYYY/MM/DD                 |
| until    | string YYYY-MM-DD or YYYY/MM/DD                 |

You will receive a deep JSON object with a lot of stuff in it. 

If you dive into it `obj.aggregations.<created_key>.buckets` you'll get a list
of timestamped usage stats. The key `key` is **epoch time** * 1000, the key
`doc_count` is the number of documents (tweets or comments) found. 

## Along the route `/activity` 
please send a `GET` with the following url params:

| KEY      | VALUE                                    |
| -------- | ---------------------------------------- |
| term     | any string your user is interested in    |
| site     | either "reddit" or "twitter"
| agg_by   | either "subreddit" or "author"           |
| since    | string YYYY-MM-DD or YYYY/MM/DD          |
| until    | string YYYY-MM-DD or YYYY/MM/DD          |

You will receive a deep JSON object with a lot of different stuff in it. 

If you dive into it `obj.aggregations.<agg_by>.buckets` this is where you'll get
numbers of post organized by your `agg_by` argument.

#### Notes: 
- `agg_by` = `author` can be a really slow query. 
- it will fail if you send it `agg_by` = `"subreddit"` and `site` = `"twitter"`, for
  reasons I hope are obvious.
- for `site` = `"twitter"`, you can actually omit `agg_by`, because "author" is
  the only option. (it defaults)

### In general: 
- It will fail if you omit `term`.
- If you submit an invalid string to `until` it will default to today. If you
  submit an invalid string to `since` it will default to two months ago.
- Heroku times out at 30 seconds. If you're sending big queries, they might fail
  for that reason. A timeout is a 503 response. 
- Please be gentle with the API-- pick like a day or a week in `since`/`until` for
test cases.
- If you can subtly discourage users from like getting hourly data for multiple
years, that would be great. Ditto doing an author aggregation over more than a
couple weeks. 
- **reddit only goes back a few months**. This is subject to change. Ask Jason if
you're having trouble.

#### sample object (`/timeseries`):  
```json
{
    "term": "election", 
    "site": "reddit", 
    "interval": "day", 
    "since": "2020-01-20", 
    "until": "2020-01-25"
}
```

#### example query (python's [requests](https://2.python-requests.org/en/master/) library): 
```python
requests.get(
    'https://smat-be.herokuapp.com/activity?&term=tornado&site=reddit&interval=month'
)
```
you can do `data=json.dumps(data)` (if you `import json`) instead of `json=data`
