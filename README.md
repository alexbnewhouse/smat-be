# SMAT Backend

The Social Media Analysis Toolkit (SMAT) was designed to help facilitate
activists, journalists, researchers, and other social good organizations to
analyze and visualize larger trends on a variety of platforms. Our intention is
to provide easy to use tools that help you tell a story about something
happening at a large scale online. The tools are designed to be quick and simple
so that users could quickly respond to things like disinformation and hate as
they occur, while also having the tools be rigorous enough to have applications
in scholarly research. While we have built the framework, it is up to you and
your curiosity to find what stories are waiting to be discovered.

## This API wraps [Pushshift](https://pushshift.io/)'s API. 

Pushshift is built with Elasticsearch, which is a pain to send queries to. This
API is easier to use, if you think sending objects through GET requests is easy. 
[View docs](DOCS.md). 


## Local Dev

### Without docker

Its recommended you create a virtual environment for installating the required packages
and the SMAT Backend:

```
virtualenv -p python3 venv
source venv/bin/activate
make install-dev
```

You should have received or asked about a `.env` file that has the right environment variables.
To set them, create a `.env` file in this repo's root directory and run:

```
source .env
```

To run SMAT backend in development mode (Flask):
```
smat-be
```

To run SMAT backend in "production" mode (wsgi):

This uses the script located at `smat_be/wsgi/smat`.
```
smat
```

## Docker

To build a `smat-be` docker container run:

```
docker build -t smat-be .
```

To run the built container:
```
docker run smat-be
```


To run the docker compose file:

```
docker-compose up
```

You can now access smat-be at `http://0.0.0.0:5000`

## Testing

While `smat-be` is running you can point queries at your local host: 

```
import json
import requests

r = requests.get(
    'http://localhost:5000/activity?&term=tornado&site=reddit&interval=month'
)
print(json.dumps(r.json(), indent=4))
```

### Testing

Run the following from the repo's root directory

```
make test
```