import os
from setuptools import setup, find_packages


def read_file(file_name):
    """
    File read wrapper for loading data unmodified from arbitrary file.
    """
    file_path = os.path.join(os.path.dirname(__file__), file_name)
    with open(file_path, "r") as fin:
        return [line for line in fin if not line.startswith(("#", "--"))]


config = dict(
    name="smat_be",
    description="SMAT backend",
    packages=find_packages(),
    version="0.0.1",
    install_requires=read_file("requirements.txt"),
    setup_requires=["setuptools_scm", "pytest-runner", "wheel"],
    package_data={"": ["*.json"]},
    include_package_data=True,
    zip_safe=False,
    scripts=["smat_be/wsgi/smat"],
    entry_points={"console_scripts": ["smat-be = smat_be.main:start"]},
)
setup(**config)
