#!/usr/bin/env python
from typing import Dict, Union
import json
from argparse import ArgumentParser
import requests  # type: ignore
from requests.models import Response  # type: ignore

HEADERS = {"content-type": "application/json"}

P = ArgumentParser(description="The service is either local or heroku. ")
P.add_argument(
    "--service",
    type=str,
    default="heroku",
    help="the service is either local or heroku. ",
)
ARGS = P.parse_args()
SERVICE = ARGS.service
URL_MAPPER = {
    "local": "http://localhost:5000/",
    "heroku": "https://smat-be.herokuapp.com/",
}
URL = URL_MAPPER[SERVICE]


def timeseries_response(url: str, data: Dict[str, Union[str, int]]) -> Response:
    if url[-1] != "/":
        url = url + "/"

    return requests.get(f"{url}timeseries", json=data, headers=HEADERS)


def act_response(url: str, data: Dict[str, Union[str, int]]) -> Response:
    if url[-1] != "/":
        url = url + "/"
    request_url = f"{url}activity?" + "&".join(f"{key}={value}" for key,value in data.items())
    return requests.get(request_url)


def content_response(url: str, data: Dict[str, Union[str, int]]) -> Response:
    if url[-1] != "/":
        url = url + "/"
    url = f"{url}content?" + "&".join([f"{k}={v}" for k, v in data.items()])

    return requests.get(url, headers=HEADERS)


ts_data = {"term": "election", "site": "gab", "interval": "month", "since": "fish"}

act_data = {
    "term": "tornado",
    "agg_by": "author",
    "site": "twitter",
    "since": "2019/10/12",
    "until": "",
}

content_data = {
    "term": "election",
    "site": "gab",
    "limit": 5,
    "since": "2019/10/12",
    "until": "",
}

if __name__ == "__main__":

    #ts_response_ = timeseries_response(url=URL, data=ts_data)
    #print(ts_response_.status_code)

    #content_response_ = content_response(url=URL, data=content_data)
    # print(json.dumps(content_response_.json(), indent=4))
    #print(content_response_.json())

    act_response_ = act_response(url=URL, data=act_data)
    act_response_dict = act_response_.json()
    print('act response: ', act_response_dict['aggregations'][act_response_dict['aggby_key']])

