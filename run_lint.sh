#!/usr/bin/env bash

mypy app
pylint app --disable=too-many-arguments
pycodestyle app --max-line-length=120
pydocstyle app
